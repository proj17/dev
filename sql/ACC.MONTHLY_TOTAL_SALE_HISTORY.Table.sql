USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [ACC].[MONTHLY_TOTAL_SALE_HISTORY]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ACC].[MONTHLY_TOTAL_SALE_HISTORY](
	[ACCOUNT_MONTH] [char](6) NOT NULL,
	[EXPECT_TOTAL_PRICE] [decimal](8, 0) NOT NULL,
	[ACTUAL_TOTAL_PRICE] [decimal](8, 0) NOT NULL,
	[TOTAL_SALE_COUNT] [int] NOT NULL,
 CONSTRAINT [PK_MONTHLY_TOTAL_SALE_HISTORY] PRIMARY KEY CLUSTERED 
(
	[ACCOUNT_MONTH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳務月份' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'MONTHLY_TOTAL_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'ACCOUNT_MONTH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預計銷售總額' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'MONTHLY_TOTAL_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'EXPECT_TOTAL_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'實際銷售總額' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'MONTHLY_TOTAL_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'ACTUAL_TOTAL_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'銷售總數' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'MONTHLY_TOTAL_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'TOTAL_SALE_COUNT'
GO
