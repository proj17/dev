﻿CREATE TABLE [ACC].[DAILY_ITEM_SALE_HISTORY] (
    [ACCOUNT_DATE] CHAR (8)      NOT NULL,
    [ITEM_NO]      NVARCHAR (50) NOT NULL,
    [ITEM_NAME]    NVARCHAR (20) NOT NULL,
    [EXPECT_PRICE] DECIMAL (8)   NOT NULL,
    [ACTUAL_PRICE] DECIMAL (8)   NOT NULL,
    [SALE_COUNT]   INT           NOT NULL,
    CONSTRAINT [PK_DAILY_ITEM_SALE_HISTORY] PRIMARY KEY CLUSTERED ([ACCOUNT_DATE] ASC, [ITEM_NO] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳務日期', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_ITEM_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'ACCOUNT_DATE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'實際銷售收入', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_ITEM_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'ACTUAL_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'預計銷售收入', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_ITEM_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'EXPECT_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品名', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_ITEM_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'ITEM_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品號', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_ITEM_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'ITEM_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銷售總數', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_ITEM_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'SALE_COUNT';

