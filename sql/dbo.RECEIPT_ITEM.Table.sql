USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [dbo].[RECEIPT_ITEM]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECEIPT_ITEM](
	[RECEIPT_NO] [nvarchar](50) NOT NULL,
	[ITEM_NO] [nvarchar](50) NOT NULL,
	[ITEM_NAME] [nvarchar](20) NOT NULL,
	[UNIT_NAME] [nvarchar](6) NOT NULL,
	[SINLE_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[ACTUAL_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[EXPECT_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[IMAGE_URL] [nvarchar](100) NULL,
	[MEMO] [nvarchar](100) NULL,
	[PARENT_RECEIPT_NO] [nvarchar](50) NULL,
	[PARENT_ITEM_NO] [nvarchar](50) NULL,
	[IS_PLUS_ITEM] [bit] NOT NULL,
	[SALE_COUNT] [int] NOT NULL,
 CONSTRAINT [PK_RECEIPT_ITEM] PRIMARY KEY CLUSTERED 
(
	[RECEIPT_NO] ASC,
	[ITEM_NO] ASC,
	[UNIT_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RECEIPT_ITEM]  WITH CHECK ADD  CONSTRAINT [FK_RECEIPT_ITEM_RECEIPT1] FOREIGN KEY([RECEIPT_NO])
REFERENCES [dbo].[RECEIPT] ([RECEIPT_NO])
GO
ALTER TABLE [dbo].[RECEIPT_ITEM] CHECK CONSTRAINT [FK_RECEIPT_ITEM_RECEIPT1]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收據單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'RECEIPT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'ITEM_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'ITEM_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單位名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'UNIT_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單一售價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'SINLE_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'實際銷售金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'ACTUAL_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預計銷售金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'EXPECT_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'圖片路徑' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'IMAGE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'MEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父銷售單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'PARENT_RECEIPT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父銷售品號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'PARENT_ITEM_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為加價購' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'IS_PLUS_ITEM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'銷售數量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT_ITEM', @level2type=N'COLUMN',@level2name=N'SALE_COUNT'
GO
