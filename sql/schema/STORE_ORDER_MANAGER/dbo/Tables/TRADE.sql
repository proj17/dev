﻿CREATE TABLE [dbo].[TRADE] (
    [TRADE_NO]                NVARCHAR (50) NOT NULL,
    [RECEIPT_NO]              NVARCHAR (50) NULL,
    [TRADE_TYPE]              TINYINT       NOT NULL,
    [ACTUAL_TOTAL_SALE_PRICE] DECIMAL (8)   NOT NULL,
    [EXPECT_TOTAL_SALE_PRICE] DECIMAL (8)   NOT NULL,
    [CREATE_DAETTIME]         DATETIME      NOT NULL,
    [CREATE_USERNAME]         NVARCHAR (10) NOT NULL,
    CONSTRAINT [PK_TRADE] PRIMARY KEY CLUSTERED ([TRADE_NO] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'實際銷售總金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE', @level2type = N'COLUMN', @level2name = N'ACTUAL_TOTAL_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE', @level2type = N'COLUMN', @level2name = N'CREATE_DAETTIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE', @level2type = N'COLUMN', @level2name = N'CREATE_USERNAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'預計銷售總金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE', @level2type = N'COLUMN', @level2name = N'EXPECT_TOTAL_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收據單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE', @level2type = N'COLUMN', @level2name = N'RECEIPT_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'交易序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE', @level2type = N'COLUMN', @level2name = N'TRADE_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'交易型態 (0 : 收入 , 1:支出)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE', @level2type = N'COLUMN', @level2name = N'TRADE_TYPE';

