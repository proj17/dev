﻿CREATE TABLE [dbo].[USER] (
    [USER_NO]         NVARCHAR (50) NOT NULL,
    [USER_NAME]       NVARCHAR (10) NOT NULL,
    [CREATE_DATETIME] DATETIME      NOT NULL,
    [CREATE_USERNAME] NVARCHAR (10) NOT NULL,
    [UPDATE_DATETIME] DATETIME      NULL,
    [UPDATE_USERNAME] NVARCHAR (10) NULL,
    [IS_ENABLED]      BIT           CONSTRAINT [DF_USER_IS_ENABLED] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_USER] PRIMARY KEY CLUSTERED ([USER_NO] ASC)
);

