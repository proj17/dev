USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [dbo].[ITEM_UINT_LIMIT]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ITEM_UINT_LIMIT](
	[ITEM_NO] [nvarchar](50) NOT NULL,
	[UNIT_NAME] [nvarchar](6) NOT NULL,
	[LIMIT_DATE] [char](8) NOT NULL,
	[LIMIT_QTY] [int] NOT NULL,
 CONSTRAINT [PK_ITEM_UINT_LIMIT_1] PRIMARY KEY CLUSTERED 
(
	[ITEM_NO] ASC,
	[UNIT_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ITEM_UINT_LIMIT]  WITH CHECK ADD  CONSTRAINT [FK_ITEM_UINT_LIMIT_ITEM_UNIT] FOREIGN KEY([ITEM_NO], [UNIT_NAME])
REFERENCES [dbo].[ITEM_UNIT] ([ITEM_NO], [UNIT_NAME])
GO
ALTER TABLE [dbo].[ITEM_UINT_LIMIT] CHECK CONSTRAINT [FK_ITEM_UINT_LIMIT_ITEM_UNIT]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UINT_LIMIT', @level2type=N'COLUMN',@level2name=N'ITEM_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單位名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UINT_LIMIT', @level2type=N'COLUMN',@level2name=N'UNIT_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'限制日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UINT_LIMIT', @level2type=N'COLUMN',@level2name=N'LIMIT_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'限制數量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UINT_LIMIT', @level2type=N'COLUMN',@level2name=N'LIMIT_QTY'
GO
