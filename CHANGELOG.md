# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.2](https://gitlab.com/proj17/dev/compare/v1.0.1...v1.0.2) (2021-07-06)


### Bug Fixes

* 測試用的 ([619cb44](https://gitlab.com/proj17/dev/commit/619cb443a882afe0c70b1e2fc017d267e49961c8))
