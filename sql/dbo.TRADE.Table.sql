USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [dbo].[TRADE]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TRADE](
	[TRADE_NO] [nvarchar](50) NOT NULL,
	[RECEIPT_NO] [nvarchar](50) NULL,
	[TRADE_TYPE] [tinyint] NOT NULL,
	[ACTUAL_TOTAL_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[EXPECT_TOTAL_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[CREATE_DAETTIME] [datetime] NOT NULL,
	[CREATE_USERNAME] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_TRADE] PRIMARY KEY CLUSTERED 
(
	[TRADE_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE', @level2type=N'COLUMN',@level2name=N'TRADE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收據單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE', @level2type=N'COLUMN',@level2name=N'RECEIPT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易型態 (0 : 收入 , 1:支出)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE', @level2type=N'COLUMN',@level2name=N'TRADE_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'實際銷售總金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE', @level2type=N'COLUMN',@level2name=N'ACTUAL_TOTAL_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預計銷售總金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE', @level2type=N'COLUMN',@level2name=N'EXPECT_TOTAL_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE', @level2type=N'COLUMN',@level2name=N'CREATE_DAETTIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE', @level2type=N'COLUMN',@level2name=N'CREATE_USERNAME'
GO
