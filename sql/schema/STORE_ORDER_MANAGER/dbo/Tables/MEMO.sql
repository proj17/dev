﻿CREATE TABLE [dbo].[MEMO] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [CONTENT]    NVARCHAR (50) NOT NULL,
    [IS_ENABLED] BIT           CONSTRAINT [DF_MEMO_IS_ENABLED] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MEMO] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'內容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MEMO', @level2type = N'COLUMN', @level2name = N'CONTENT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代號 (識別規格)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MEMO', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否停用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MEMO', @level2type = N'COLUMN', @level2name = N'IS_ENABLED';

