﻿CREATE TABLE [dbo].[ITEM_UINT_LIMIT] (
    [ITEM_NO]    NVARCHAR (50) NOT NULL,
    [UNIT_NAME]  NVARCHAR (6)  NOT NULL,
    [LIMIT_DATE] CHAR (8)      NOT NULL,
    [LIMIT_QTY]  INT           NOT NULL,
    CONSTRAINT [PK_ITEM_UINT_LIMIT_1] PRIMARY KEY CLUSTERED ([ITEM_NO] ASC, [UNIT_NAME] ASC),
    CONSTRAINT [FK_ITEM_UINT_LIMIT_ITEM_UNIT] FOREIGN KEY ([ITEM_NO], [UNIT_NAME]) REFERENCES [dbo].[ITEM_UNIT] ([ITEM_NO], [UNIT_NAME])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UINT_LIMIT', @level2type = N'COLUMN', @level2name = N'ITEM_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'限制日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UINT_LIMIT', @level2type = N'COLUMN', @level2name = N'LIMIT_DATE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'限制數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UINT_LIMIT', @level2type = N'COLUMN', @level2name = N'LIMIT_QTY';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單位名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UINT_LIMIT', @level2type = N'COLUMN', @level2name = N'UNIT_NAME';

