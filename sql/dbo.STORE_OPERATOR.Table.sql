USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [dbo].[STORE_OPERATOR]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STORE_OPERATOR](
	[ID] [int] NOT NULL,
	[START_DATETIME] [datetime] NOT NULL,
	[END_DATETIME] [datetime] NULL,
 CONSTRAINT [PK_STORE_OPERATOR] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'識別規格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STORE_OPERATOR', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'開店時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STORE_OPERATOR', @level2type=N'COLUMN',@level2name=N'START_DATETIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'關店時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STORE_OPERATOR', @level2type=N'COLUMN',@level2name=N'END_DATETIME'
GO
