﻿CREATE TABLE [dbo].[ITEM_UNIT] (
    [ITEM_NO]           NVARCHAR (50)  NOT NULL,
    [UNIT_NAME]         NVARCHAR (6)   NOT NULL,
    [SINGLE_SALE_PRICE] DECIMAL (8)    NOT NULL,
    [IMAGE_URL]         NVARCHAR (100) NULL,
    [IS_ENABLED]        BIT            CONSTRAINT [DF_ITEM_UNIT_IS_ENABLED] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ITEM_UNIT_1] PRIMARY KEY CLUSTERED ([ITEM_NO] ASC, [UNIT_NAME] ASC),
    CONSTRAINT [FK_ITEM_UNIT_ITEM] FOREIGN KEY ([ITEM_NO]) REFERENCES [dbo].[ITEM] ([ITEM_NO])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'圖片路徑', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UNIT', @level2type = N'COLUMN', @level2name = N'IMAGE_URL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否停用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UNIT', @level2type = N'COLUMN', @level2name = N'IS_ENABLED';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UNIT', @level2type = N'COLUMN', @level2name = N'ITEM_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單一售價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UNIT', @level2type = N'COLUMN', @level2name = N'SINGLE_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM_UNIT', @level2type = N'COLUMN', @level2name = N'UNIT_NAME';

