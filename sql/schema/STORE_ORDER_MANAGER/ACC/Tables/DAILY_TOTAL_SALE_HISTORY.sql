﻿CREATE TABLE [ACC].[DAILY_TOTAL_SALE_HISTORY] (
    [ACCOUNT_DATE]       CHAR (8)    NOT NULL,
    [EXPECT_TOTAL_PRICE] DECIMAL (8) NOT NULL,
    [ACTUAL_TOTAL_PRICE] DECIMAL (8) NOT NULL,
    [TOTAL_SALE_COUNT]   INT         NOT NULL,
    CONSTRAINT [PK_DAILY_TOTAL_SALE_HISTORY] PRIMARY KEY CLUSTERED ([ACCOUNT_DATE] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳務日期', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_TOTAL_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'ACCOUNT_DATE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'實際銷售總金額', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_TOTAL_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'ACTUAL_TOTAL_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'預計銷售總金額', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_TOTAL_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'EXPECT_TOTAL_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銷售總數', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'DAILY_TOTAL_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'TOTAL_SALE_COUNT';

