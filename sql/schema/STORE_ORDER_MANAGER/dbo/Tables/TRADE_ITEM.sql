﻿CREATE TABLE [dbo].[TRADE_ITEM] (
    [TRADE_NO]          NVARCHAR (50) NOT NULL,
    [RECEIPT_NO]        NVARCHAR (50) NULL,
    [ITEM_NO]           NVARCHAR (50) NOT NULL,
    [ITEM_NAME]         NVARCHAR (20) NOT NULL,
    [UNIT_NAME]         NVARCHAR (6)  NOT NULL,
    [ACTUAL_SALE_PRICE] DECIMAL (8)   NOT NULL,
    [EXPECT_SAEL_PRICE] DECIMAL (8)   NOT NULL,
    [IS_PLUS_ITEM]      BIT           NOT NULL,
    [SALE_COUNT]        INT           NOT NULL,
    CONSTRAINT [PK_TRADE_ITEM] PRIMARY KEY CLUSTERED ([TRADE_NO] ASC, [ITEM_NO] ASC, [UNIT_NAME] ASC),
    CONSTRAINT [FK_TRADE_ITEM_TRADE] FOREIGN KEY ([TRADE_NO]) REFERENCES [dbo].[TRADE] ([TRADE_NO])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'實際銷售金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'ACTUAL_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'預計銷售金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'EXPECT_SAEL_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否為加購項目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'IS_PLUS_ITEM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'ITEM_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'ITEM_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收據單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'RECEIPT_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銷售數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'SALE_COUNT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'交易序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'TRADE_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單位名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRADE_ITEM', @level2type = N'COLUMN', @level2name = N'UNIT_NAME';

