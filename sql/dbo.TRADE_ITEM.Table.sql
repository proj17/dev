USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [dbo].[TRADE_ITEM]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TRADE_ITEM](
	[TRADE_NO] [nvarchar](50) NOT NULL,
	[RECEIPT_NO] [nvarchar](50) NULL,
	[ITEM_NO] [nvarchar](50) NOT NULL,
	[ITEM_NAME] [nvarchar](20) NOT NULL,
	[UNIT_NAME] [nvarchar](6) NOT NULL,
	[ACTUAL_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[EXPECT_SAEL_PRICE] [decimal](8, 0) NOT NULL,
	[IS_PLUS_ITEM] [bit] NOT NULL,
	[SALE_COUNT] [int] NOT NULL,
 CONSTRAINT [PK_TRADE_ITEM] PRIMARY KEY CLUSTERED 
(
	[TRADE_NO] ASC,
	[ITEM_NO] ASC,
	[UNIT_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TRADE_ITEM]  WITH CHECK ADD  CONSTRAINT [FK_TRADE_ITEM_TRADE] FOREIGN KEY([TRADE_NO])
REFERENCES [dbo].[TRADE] ([TRADE_NO])
GO
ALTER TABLE [dbo].[TRADE_ITEM] CHECK CONSTRAINT [FK_TRADE_ITEM_TRADE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'TRADE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收據單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'RECEIPT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'ITEM_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'ITEM_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單位名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'UNIT_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'實際銷售金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'ACTUAL_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預計銷售金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'EXPECT_SAEL_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為加購項目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'IS_PLUS_ITEM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'銷售數量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TRADE_ITEM', @level2type=N'COLUMN',@level2name=N'SALE_COUNT'
GO
