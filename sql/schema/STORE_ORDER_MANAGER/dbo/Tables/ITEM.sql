﻿CREATE TABLE [dbo].[ITEM] (
    [ITEM_NO]    NVARCHAR (50)  NOT NULL,
    [ITEM_NAME]  NVARCHAR (20)  NOT NULL,
    [IMAGE_URL]  NVARCHAR (100) NULL,
    [MEMO]       NVARCHAR (50)  NULL,
    [IS_ENABLED] BIT            CONSTRAINT [DF_ITEM_IS_ENABLED] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ITEM] PRIMARY KEY CLUSTERED ([ITEM_NO] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'商品圖片路徑', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM', @level2type = N'COLUMN', @level2name = N'IMAGE_URL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否停用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM', @level2type = N'COLUMN', @level2name = N'IS_ENABLED';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM', @level2type = N'COLUMN', @level2name = N'ITEM_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM', @level2type = N'COLUMN', @level2name = N'ITEM_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'商品備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ITEM', @level2type = N'COLUMN', @level2name = N'MEMO';

