﻿CREATE TABLE [dbo].[RECEIPT_ITEM] (
    [RECEIPT_NO]        NVARCHAR (50)  NOT NULL,
    [ITEM_NO]           NVARCHAR (50)  NOT NULL,
    [ITEM_NAME]         NVARCHAR (20)  NOT NULL,
    [UNIT_NAME]         NVARCHAR (6)   NOT NULL,
    [SINLE_SALE_PRICE]  DECIMAL (8)    NOT NULL,
    [ACTUAL_SALE_PRICE] DECIMAL (8)    NOT NULL,
    [EXPECT_SALE_PRICE] DECIMAL (8)    NOT NULL,
    [IMAGE_URL]         NVARCHAR (100) NULL,
    [MEMO]              NVARCHAR (100) NULL,
    [PARENT_RECEIPT_NO] NVARCHAR (50)  NULL,
    [PARENT_ITEM_NO]    NVARCHAR (50)  NULL,
    [IS_PLUS_ITEM]      BIT            NOT NULL,
    [SALE_COUNT]        INT            NOT NULL,
    CONSTRAINT [PK_RECEIPT_ITEM] PRIMARY KEY CLUSTERED ([RECEIPT_NO] ASC, [ITEM_NO] ASC, [UNIT_NAME] ASC),
    CONSTRAINT [FK_RECEIPT_ITEM_RECEIPT1] FOREIGN KEY ([RECEIPT_NO]) REFERENCES [dbo].[RECEIPT] ([RECEIPT_NO])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'實際銷售金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'ACTUAL_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'預計銷售金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'EXPECT_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'圖片路徑', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'IMAGE_URL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否為加價購', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'IS_PLUS_ITEM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'ITEM_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'ITEM_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'MEMO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'父銷售品號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'PARENT_ITEM_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'父銷售單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'PARENT_RECEIPT_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收據單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'RECEIPT_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銷售數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'SALE_COUNT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單一售價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'SINLE_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單位名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT_ITEM', @level2type = N'COLUMN', @level2name = N'UNIT_NAME';

