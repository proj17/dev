﻿CREATE TABLE [dbo].[RECEIPT] (
    [RECEIPT_NO]              NVARCHAR (50)  NOT NULL,
    [CATCH_TYPE]              TINYINT        NOT NULL,
    [EXPECT_FINISH_DATETIME]  DATETIME       NULL,
    [SELF_CATCH_DATETIME]     DATETIME       NULL,
    [ADDRESS]                 NVARCHAR (100) NULL,
    [TEL]                     NVARCHAR (10)  NULL,
    [MEMO]                    NVARCHAR (100) NULL,
    [ACTUAL_TOTAL_SALE_PRICE] DECIMAL (8)    NOT NULL,
    [EXPECT_TOTAL_SALE_PRICE] DECIMAL (8)    NOT NULL,
    [IS_CALL_IN]              BIT            NOT NULL,
    [CREATE_DAETTIME]         DATETIME       NOT NULL,
    [CREATE_USERNAME]         NVARCHAR (10)  NOT NULL,
    [UPDATE_DAETTIME]         DATETIME       NULL,
    [UPDATE_USERNAME]         NVARCHAR (10)  NULL,
    CONSTRAINT [PK_RECEIPT] PRIMARY KEY CLUSTERED ([RECEIPT_NO] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'實際銷售總額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'ACTUAL_TOTAL_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'聯絡地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'ADDRESS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'內用或外帶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'CATCH_TYPE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'CREATE_DAETTIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'CREATE_USERNAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'預計完成時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'EXPECT_FINISH_DATETIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'預計銷售總額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'EXPECT_TOTAL_SALE_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否來電', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'IS_CALL_IN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備註', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'MEMO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收據單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'RECEIPT_NO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'自拿時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'SELF_CATCH_DATETIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'連絡電話', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'TEL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'UPDATE_DAETTIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RECEIPT', @level2type = N'COLUMN', @level2name = N'UPDATE_USERNAME';

