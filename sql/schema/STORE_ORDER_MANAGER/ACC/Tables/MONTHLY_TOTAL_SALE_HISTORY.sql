﻿CREATE TABLE [ACC].[MONTHLY_TOTAL_SALE_HISTORY] (
    [ACCOUNT_MONTH]      CHAR (6)    NOT NULL,
    [EXPECT_TOTAL_PRICE] DECIMAL (8) NOT NULL,
    [ACTUAL_TOTAL_PRICE] DECIMAL (8) NOT NULL,
    [TOTAL_SALE_COUNT]   INT         NOT NULL,
    CONSTRAINT [PK_MONTHLY_TOTAL_SALE_HISTORY] PRIMARY KEY CLUSTERED ([ACCOUNT_MONTH] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳務月份', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'MONTHLY_TOTAL_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'ACCOUNT_MONTH';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'實際銷售總額', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'MONTHLY_TOTAL_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'ACTUAL_TOTAL_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'預計銷售總額', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'MONTHLY_TOTAL_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'EXPECT_TOTAL_PRICE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銷售總數', @level0type = N'SCHEMA', @level0name = N'ACC', @level1type = N'TABLE', @level1name = N'MONTHLY_TOTAL_SALE_HISTORY', @level2type = N'COLUMN', @level2name = N'TOTAL_SALE_COUNT';

