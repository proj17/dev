USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [dbo].[RECEIPT]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECEIPT](
	[RECEIPT_NO] [nvarchar](50) NOT NULL,
	[CATCH_TYPE] [tinyint] NOT NULL,
	[EXPECT_FINISH_DATETIME] [datetime] NULL,
	[SELF_CATCH_DATETIME] [datetime] NULL,
	[ADDRESS] [nvarchar](100) NULL,
	[TEL] [nvarchar](10) NULL,
	[MEMO] [nvarchar](100) NULL,
	[ACTUAL_TOTAL_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[EXPECT_TOTAL_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[IS_CALL_IN] [bit] NOT NULL,
	[CREATE_DAETTIME] [datetime] NOT NULL,
	[CREATE_USERNAME] [nvarchar](10) NOT NULL,
	[UPDATE_DAETTIME] [datetime] NULL,
	[UPDATE_USERNAME] [nvarchar](10) NULL,
 CONSTRAINT [PK_RECEIPT] PRIMARY KEY CLUSTERED 
(
	[RECEIPT_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收據單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'RECEIPT_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'內用或外帶' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'CATCH_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預計完成時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'EXPECT_FINISH_DATETIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自拿時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'SELF_CATCH_DATETIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'聯絡地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'ADDRESS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'連絡電話' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'TEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'MEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'實際銷售總額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'ACTUAL_TOTAL_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預計銷售總額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'EXPECT_TOTAL_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否來電' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'IS_CALL_IN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'CREATE_DAETTIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'CREATE_USERNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'UPDATE_DAETTIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RECEIPT', @level2type=N'COLUMN',@level2name=N'UPDATE_USERNAME'
GO
