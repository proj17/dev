USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [dbo].[MEMO]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MEMO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CONTENT] [nvarchar](50) NOT NULL,
	[IS_ENABLED] [bit] NOT NULL,
 CONSTRAINT [PK_MEMO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MEMO] ADD  CONSTRAINT [DF_MEMO_IS_ENABLED]  DEFAULT ((1)) FOR [IS_ENABLED]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代號 (識別規格)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MEMO', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'內容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MEMO', @level2type=N'COLUMN',@level2name=N'CONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否停用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MEMO', @level2type=N'COLUMN',@level2name=N'IS_ENABLED'
GO
