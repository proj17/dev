USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [dbo].[ITEM_UNIT]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ITEM_UNIT](
	[ITEM_NO] [nvarchar](50) NOT NULL,
	[UNIT_NAME] [nvarchar](6) NOT NULL,
	[SINGLE_SALE_PRICE] [decimal](8, 0) NOT NULL,
	[IMAGE_URL] [nvarchar](100) NULL,
	[IS_ENABLED] [bit] NOT NULL,
 CONSTRAINT [PK_ITEM_UNIT_1] PRIMARY KEY CLUSTERED 
(
	[ITEM_NO] ASC,
	[UNIT_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ITEM_UNIT] ADD  CONSTRAINT [DF_ITEM_UNIT_IS_ENABLED]  DEFAULT ((1)) FOR [IS_ENABLED]
GO
ALTER TABLE [dbo].[ITEM_UNIT]  WITH CHECK ADD  CONSTRAINT [FK_ITEM_UNIT_ITEM] FOREIGN KEY([ITEM_NO])
REFERENCES [dbo].[ITEM] ([ITEM_NO])
GO
ALTER TABLE [dbo].[ITEM_UNIT] CHECK CONSTRAINT [FK_ITEM_UNIT_ITEM]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UNIT', @level2type=N'COLUMN',@level2name=N'ITEM_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UNIT', @level2type=N'COLUMN',@level2name=N'UNIT_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'單一售價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UNIT', @level2type=N'COLUMN',@level2name=N'SINGLE_SALE_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'圖片路徑' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UNIT', @level2type=N'COLUMN',@level2name=N'IMAGE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否停用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEM_UNIT', @level2type=N'COLUMN',@level2name=N'IS_ENABLED'
GO
