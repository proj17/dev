﻿CREATE TABLE [dbo].[STORE_OPERATOR] (
    [ID]             INT      NOT NULL,
    [START_DATETIME] DATETIME NOT NULL,
    [END_DATETIME]   DATETIME NULL,
    CONSTRAINT [PK_STORE_OPERATOR] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'關店時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'STORE_OPERATOR', @level2type = N'COLUMN', @level2name = N'END_DATETIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'識別規格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'STORE_OPERATOR', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'開店時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'STORE_OPERATOR', @level2type = N'COLUMN', @level2name = N'START_DATETIME';

