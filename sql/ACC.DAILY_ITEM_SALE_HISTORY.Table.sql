USE [STORE_ORDER_MANAGER]
GO
/****** Object:  Table [ACC].[DAILY_ITEM_SALE_HISTORY]    Script Date: 2021/7/6 下午 11:29:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ACC].[DAILY_ITEM_SALE_HISTORY](
	[ACCOUNT_DATE] [char](8) NOT NULL,
	[ITEM_NO] [nvarchar](50) NOT NULL,
	[ITEM_NAME] [nvarchar](20) NOT NULL,
	[EXPECT_PRICE] [decimal](8, 0) NOT NULL,
	[ACTUAL_PRICE] [decimal](8, 0) NOT NULL,
	[SALE_COUNT] [int] NOT NULL,
 CONSTRAINT [PK_DAILY_ITEM_SALE_HISTORY] PRIMARY KEY CLUSTERED 
(
	[ACCOUNT_DATE] ASC,
	[ITEM_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帳務日期' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'DAILY_ITEM_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'ACCOUNT_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品號' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'DAILY_ITEM_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'ITEM_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品名' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'DAILY_ITEM_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'ITEM_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預計銷售收入' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'DAILY_ITEM_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'EXPECT_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'實際銷售收入' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'DAILY_ITEM_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'ACTUAL_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'銷售總數' , @level0type=N'SCHEMA',@level0name=N'ACC', @level1type=N'TABLE',@level1name=N'DAILY_ITEM_SALE_HISTORY', @level2type=N'COLUMN',@level2name=N'SALE_COUNT'
GO
